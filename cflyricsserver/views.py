# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from random import choice

__author__ = 'Danny Goodall'

from cflyricsserver import app, log, api, lyrics
from flask import Flask, abort, render_template
from flask.ext import restful
from flask import render_template
from cflyricsserver.errors import CFLyricsScrapingError

from cflyricsserver.config import NULL_ARTIST_SEARCH, NULL_LYRICS, NULL_SONGS

LYRIC_LIST = [
    (
        "billy bragg",
        "days like these"
    ),
    (
        "the decemberists",
        "the bagman's gambit"
    ),
    (
        "the decemberists",
        "a cautionary song"
    ),
    (
        "the decemberists",
        "my mother was a chinese trapeze artist"
    ),
    (
        "darren hanlon",
        "fire engine"
    ),
    (
        "darren hanlon",
        "manilla nsw"
    ),
    (
        "darren hanlon",
        "the ostracism of vinny lalor"
    ),
    (
        "darren hanlon",
        "happiness is a chemical"
    ),
    (
        "darren hanlon",
        "spend christmas day with me"
    ),
    (
        "darren hanlon",
        "old dream"
    ),
    (
        "darren hanlon",
        "butterfly bones"
    ),
    (
        "darren hanlon",
        "all these things"
    ),
    (
        "darren hanlon",
        "folk insomnia"
    ),

]

# Define the route for the index
@app.route('/')
def index():

    artist, title = choice(LYRIC_LIST)
    lyric_dict = lyrics.get_lyrics(artist, title, stripped=False)
    return render_template(
        'index.html',
        **{
            "lyrics": lyric_dict['lyrics'],
            "artist": " ".join(w.capitalize() for w in artist.split()),
            "title": " ".join(w.capitalize() for w in title.split())
        }
    )


# Define the REST API Handler - only need the .get method
class ReturnLyrics(restful.Resource):
    def get(self, artist=None, title=None):
        # Scrape the page and return the JSON document as a Python dictionary.
        # Except an error to occur as it seems that the BBC site suffers with transient errors. At some point in the
        # future I will build in a way of caching the results for a week

        log.debug("  Entered ReturnChart")

        # Validate that the parameters are in range
        if artist is None or not artist or title is None or not title:
            abort(404)

        try:
            lyric_dict = lyrics.get_lyrics(artist, title)

        except CFLyricsScrapingError as e:
            # OK so we got a scraping error from the wikia  site, so let's return a null version
            lyric_dict = NULL_LYRICS

        # Return the dictionary and let Flask-RESTful do the conversion
        return lyric_dict


# Define the REST API Handler - only need the .get method
class ReturnSongs(restful.Resource):
    def get(self, artist=None):
        # Scrape the page and return the JSON document as a Python dictionary.
        # Except an error to occur as it seems that the BBC site suffers with transient errors. At some point in the
        # future I will build in a way of caching the results for a week

        # Validate that the parameters are in range
        if artist is None or not artist:
            abort(404)

        try:
            songs_dict = lyrics.get_artist_songs(artist)

        except CFLyricsScrapingError as e:
            # OK so we got a scraping error from the wikia  site, so let's return a null version
            songs_dict = NULL_SONGS

        # Return the dictionary and let Flask-RESTful do the conversion
        return songs_dict


# Define the REST API Handler - only need the .get method
class ReturnSearchArtist(restful.Resource):
    def get(self, artist=None):
        # Scrape the page and return the JSON document as a Python dictionary.
        # Except an error to occur as it seems that the BBC site suffers with transient errors. At some point in the
        # future I will build in a way of caching the results for a week

        # Validate that the parameters are in range
        if artist is None or not artist:
            abort(404)

        try:
            search_artist_dict = lyrics.search_for_artist(artist)

        except CFLyricsScrapingError as e:
            # OK so we got a scraping error from the wikia  site, so let's return a null version
            songs_dict = NULL_ARTIST_SEARCH

        # Return the dictionary and let Flask-RESTful do the conversion
        return search_artist_dict

api.add_resource(ReturnLyrics, '/lyricsapi/lyrics/<artist>/<title>/')
api.add_resource(ReturnSongs, '/lyricsapi/songs/<artist>/')
api.add_resource(ReturnSearchArtist, '/lyricsapi/search/<artist>/')

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/500')
def server_400():
    abort(500)

@app.route('/404')
def server_404():
    abort(404)