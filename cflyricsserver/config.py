# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'Danny Goodall'
from cflyricsserver.utils import env_value

DEBUG=env_value("DEBUG",False)

# Are we running on the Heroku platform (Requires an environment variable to be set on Heroku first!)
ON_HEROKU = env_value("ON_HEROKU", False)

NULL_LYRICS = dict(
    lyrics=list()
)
NULL_SONGS = dict(
    songs=list()
)
NULL_ARTIST_SEARCH = dict(
    artist=dict(
        artist='',
        url=''
    )
)