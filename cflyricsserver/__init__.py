# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from html.parser import HTMLParseError
import bs4
from cflyricsserver.helpers import MnLyrics

from flask import Flask
from flask.ext import restful
from flask.ext.pymongo import PyMongo
from flask_bootstrap import Bootstrap
import logbook
from logbook import NullHandler, StderrHandler, NestedSetup, Logger
from cflyricsserver.errors import CFLyricsScrapingError

from cflyricsserver.utils import env_value
from cflyricsserver.config import DEBUG, ON_HEROKU


# Create the Flask App
import requests

app = Flask(__name__)

# Turn debug mode on or off
app.config["DEBUG"] = DEBUG

# Create our REST API instance
api = restful.Api(app)

# Let's create an error handler
error_handlers = NestedSetup(
    [
        NullHandler(),
        StderrHandler(level=logbook.NOTICE if not app.config['DEBUG'] else logbook.DEBUG)
    ]
)
log = Logger("CFLyricsServer", level=logbook.DEBUG if DEBUG else logbook.NOTICE)

# Initialise Bootstrap templates
bootstrap = Bootstrap(app)

# Initialise the lyric interface

lyrics = MnLyrics(from_string='danny@onebloke.com')

import cflyricsserver.views