# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from lxml.etree import ParserError
from cflyricsserver.config import NULL_LYRICS, NULL_SONGS, NULL_ARTIST_SEARCH
from cflyricsserver.errors import CFLyricsScrapingError
from lxml import html, etree
from lxml.html.clean import clean_html
import requests, requests.exceptions
from nap.url import Url

__author__ = 'Danny Goodall'

class MnLyricsAPI(Url):

    def __init__(self, base_url, **default_kwargs):
        # Did we pass a header in that should be passed to the remote server on each API call
        self.headers = default_kwargs['headers'] if "headers" in default_kwargs else {}

        # Call parent init
        super(MnLyricsAPI,self).__init__(base_url, **default_kwargs)

    def after_request(self, response):
        if response.status_code != 200:
            response.raise_for_status()

        return response.json()

    def before_request(self, method, relative_url, request_kwargs):
        # Specify that we want the format in json - notice we use realjson due to an error in the original json
        # implementation
        request_kwargs['params']['fmt'] = 'realjson'

        # Send any headers that were passed when this instance was created - typically User-Agent and From
        request_kwargs['headers'] = self.headers

        return request_kwargs


class MnLyrics:
    def __init__(self, user_agent="cflyrics V1.0 - CodeFurther Lyrics a non-commercial API for teaching kids how to code.", from_string=None):
        self.url = "http://lyrics.wikia.com/api.php"
        self.user_agent = None
        self.from_string = None
        self.headers = {}
        self.set_header(user_agent, from_string)
        self.api = MnLyricsAPI(self.url, **dict(headers=self.headers))

    def set_header(self, user_agent, from_string):
        self.user_agent = user_agent
        self.from_string = from_string
        if self.user_agent:
            self.headers['User-Agent'] = self.user_agent
        if self.from_string:
            self.headers['From'] = self.from_string

    def _get_page_html(self, page, headers=None, params=None):

        if headers is None:
            headers = self.headers
        if params is None:
            params=dict()

        # There are many errors that can occur whilst we're trying to get the stuff from the BBC site
        # Let's catch them and then re-raise them as our own Top40ScrapingError
        try:
            html_request = requests.get(page, headers=headers, allow_redirects=True, params=params)
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout) as e:
            raise CFLyricsScrapingError("A connection error or connection timeout error occurred.") from e
        except requests.exceptions.HTTPError as e:
            raise CFLyricsScrapingError("An HTTP error was returned from the remote server") from e

        if html_request.status_code == 404:
            raise CFLyricsScrapingError("Page not found for that artist.")

        # We could get parse errors in the HTML, so let's raise a Top40ScrapingError if we do
        try:
            page_html = html.fromstring(html_request.text)
        except (UnicodeEncodeError, KeyError, AttributeError, ParserError) as e:
            raise CFLyricsScrapingError("Couldn't parse the text from " + page) from e

        return page_html


    def get_lyrics_from_page(self, page, stripped):

        page_html = self._get_page_html(page)

        # Find all table rows in the HTML document
        lyric_divs = page_html.cssselect("div.lyricbox")
        if not len(lyric_divs):
            list()

        lyric_div = lyric_divs[0]

        # Code inspired from here https://github.com/ysim/songtext/blob/master/libsongtext/lyricwiki.py

        for br in lyric_div.cssselect('br'):
            br.tail = '\n' + br.tail if br.tail else '\n'
        etree.strip_elements(lyric_div, 'br', with_tail=False)

        bad_tags = lyric_div.cssselect('script') + lyric_div.cssselect('.lyricsbreak') + lyric_div.cssselect('.rtMatcher')
        for tag in bad_tags:
            tag.drop_tree()

        real_string = etree.tostring(lyric_div, encoding='unicode')
        cleaned_html = clean_html(real_string)
        text_content = "{}".format(
            html.fragment_fromstring(cleaned_html).text_content()
        )

        # Now convert the lyrics to a list of text
        lyrics = [x for x in text_content.split('\n')]

        return lyrics

    def get_lyrics(self, artist, song, stripped=True):
        json_response = self.api.get(params={"artist": artist, "song": song})
        # Did we get a match?
        if json_response["lyrics"].lower().startswith("not found"):
            lyrics = list()
        else:
            lyrics = self.get_lyrics_from_page(json_response['url'], stripped=stripped)

        lyrics_dict = dict(lyrics=lyrics)

        return lyrics_dict

    def get_artist_songs(self, artist):

        songs_dict = NULL_SONGS
        # Set the page for the artist's songs
        page = 'http://lyrics.wikia.com/{}:'.format(
            artist
        )

        page_html = self._get_page_html(page)

        # Find all table rows in the HTML document
        song_titles = page_html.cssselect("li > b:nth-child(1) > a:nth-child(1)")

        for anchor in song_titles:
            songs_dict['songs'].append(
                dict(
                    song = anchor.text,
                    url = anchor.attrib['href'],
                    title = anchor.attrib['title']
                )
            )

        return songs_dict

    def search_for_artist(self, artist):

        artist_dict = NULL_ARTIST_SEARCH

        page_html = self._get_page_html(
            'http://lyrics.wikia.com/Special:Search',
            params={
                'search': artist,
                'fulltext': 'Search'
            }
        )

        artist_links = page_html.cssselect('ul.Results > li:nth-child(1).result > article:nth-child(1) > h1:nth-child(1) > a:nth-child(1).result-link')

        if not len(artist_links):
            return artist_dict

        artist_link = artist_links[0]

        artist_dict['artist']['artist'] = artist_link.text
        artist_dict['artist']['url'] = artist_link.attrib['href']

        return artist_dict


