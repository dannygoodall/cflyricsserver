CFLyricsServer
==============

The **CFLyricsServer** library is designed to serve requests from the `cflyrics module
<https://bitbucket.org/dannygoodall/cflyrics>`_ to return song and lyric search results from the MusicnLyrics database.

Changes
=======

v0.1.0.dev1 - 16th December 2014
--------------------------------
* Initial version

